package gui.controllers;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Optional;

public class AppStartController {
    @FXML
    private Button parseNewForumButton;
    @FXML
    private Button forumTreeViewerButton;
    public void initialize(){
        forumTreeViewerButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage newStage = new Stage(StageStyle.DECORATED);
                FXMLLoader loader = new FXMLLoader(
                        getClass()
                        .getResource("/fxml/ForumTreeViewer.fxml"
                        )
                );
                try {
                    newStage.setScene(new Scene(loader.load()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                newStage.show();
            }
        });
        parseNewForumButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = (Stage) parseNewForumButton.getScene().getWindow();
                TextInputDialog dialog = new TextInputDialog("https://www.phpbb.com/community/");
                dialog.setTitle("Parsing new forum...");
                dialog.setHeaderText("Enter essential information");
                dialog.setContentText("Please enter forum url:");
                Optional<String> result = dialog.showAndWait();
                if(result.isPresent()) {
                    Stage parserStage = new Stage(StageStyle.DECORATED);
                    FXMLLoader loader = new FXMLLoader(
                            getClass()
                                    .getResource("/fxml/NewForumParser.fxml"
                                    )
                    );
                    try {
                        parserStage.setScene(new Scene(loader.load()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    NewForumParserController controller = loader.<NewForumParserController>getController();
                    controller.setTextField(result.get());
                    parserStage.show();

                }
            }
        });
    }
}
