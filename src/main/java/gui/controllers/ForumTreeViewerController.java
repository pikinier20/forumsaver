package gui.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.ContextMenuEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import parser.db.DatabaseService;
import parser.models.elements.Forum;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ForumTreeViewerController {
    @FXML
    private TreeView<String> tree;

    private Map<String,Long> forumCounters;


    public void initialize(){
        decorateTree(tree);
        TreeItem<String> rootItem = new TreeItem<String> ("Database:");
        var dbService = new DatabaseService();
        forumCounters = dbService.getTopicCount();
        var children = dbService.getRootNodes();
        for(var child : children) {
            var node = new ExtendedTreeItem(child);
            node.getChildren().addAll(buildChildren(node.associatedForum));
            rootItem.getChildren().add(node);
        }
        rootItem.setExpanded(true);
        tree.setRoot(rootItem);
    }
    private List<ExtendedTreeItem> buildChildren(Forum forum){
        var result = new LinkedList<ExtendedTreeItem>();
        forum.getSubforums().forEach(a -> result.add(new ExtendedTreeItem(a)));
        for(var child : result){
            Long count = new Long(0);
            if(forumCounters.containsKey(child.associatedForum.getForumUrl())){
                count = forumCounters.get(child.associatedForum.getForumUrl());
            }
            child.setValue(child.getValue() + "[" + count + "]");
            child.getChildren().addAll(buildChildren(child.associatedForum));
        }
        return result;
    }
    private void decorateTree(TreeView<String> tree){
        var contextMenu = this.getContextMenu();
        tree.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent contextMenuEvent) {
                contextMenu.show(tree, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());

            }
        });
    }
    private ContextMenu getContextMenu(){
        var result = new ContextMenu();
        MenuItem parseTopicItem = new MenuItem("Parse topics");
        parseTopicItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                var forumNode = (ExtendedTreeItem) tree.getSelectionModel().getSelectedItem();
                parseTopics(forumNode.associatedForum);
            }
        });
        MenuItem showParsedTopics = new Menu("Show topics");
        showParsedTopics.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                var forumNode = (ExtendedTreeItem) tree.getSelectionModel().getSelectedItem();
                showTopics(forumNode.associatedForum);
            }
        });
        result.getItems().add(parseTopicItem);
        result.getItems().add(showParsedTopics);
        return result;
    }

    private void parseTopics(Forum forum) {
        Stage parserStage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(
                getClass()
                        .getResource("/fxml/TopicParser.fxml"
                        )
        );
        try {
            parserStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        TopicParserController controller = loader.<TopicParserController>getController();
        controller.setForum(forum);
        parserStage.show();
    }
    private void showTopics(Forum forum){
        Stage parserStage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(
                getClass()
                        .getResource("/fxml/TopicList.fxml"
                        )
        );
        try {
            parserStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        TopicListController controller = loader.<TopicListController>getController();
        controller.setForum(forum);
        parserStage.show();
    }
}

class ExtendedTreeItem extends TreeItem<String>{
    Forum associatedForum;
    ExtendedTreeItem(Forum forum){
        super(forum.getName());
        this.associatedForum = forum;
    }
}
