package gui.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.concurrent.Task;
import parser.DataParser;
import parser.db.DatabaseService;
import parser.models.elements.Forum;


public class NewForumParserController {
    @FXML
    private TextField inputTextField;
    @FXML
    private Button startButton;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label progressBarLabel;
    public void initialize(){
        startButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                startParse();
            }
        });
    }
    public void setTextField(String s){
        this.inputTextField.setText(s);
    }
    public void startParse(){
        var dataParser = new DataParser();
        var databaseService = new DatabaseService();
        var task = new Task<Void>(){
            @Override
            protected Void call() throws Exception {
                updateProgress(0,100);
                updateProgress(33,100);
                updateMessage("Parsing forum...");
                Forum result = null;
                result = dataParser.parseForumStructure(inputTextField.getText());
                updateProgress(66,100);
                updateMessage("Saving forum in database...");
                if(result != null) {
                    databaseService.mergeData(result);
                    updateProgress(100,100);
                    updateMessage("Forum parsed successfully");
                }
                else{
                    updateProgress(0,100);
                    updateMessage("Error during database parse");
                    Platform.runLater(() -> getAlert());
                }
            return null;
            }
        };

        progressBar.progressProperty().bind(task.progressProperty());
        progressBarLabel.textProperty().bind(task.messageProperty());
        new Thread(task).start();
    }
    public void getAlert(){
        Alert alert = null;
        try {
            alert = new Alert(Alert.AlertType.ERROR);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        alert.setContentText("Provided URL does not contain valid phpBB forum. Remember that you need to provide full URL (with http/https etc.).");
        alert.setHeaderText("Error during database parse.");
        alert.initOwner(startButton.getScene().getWindow());
        alert.show();
    }
}
