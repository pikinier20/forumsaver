package gui.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import parser.db.DatabaseService;
import parser.models.elements.Post;
import parser.models.elements.Topic;

import java.util.List;

public class PostCreatorController {
    @FXML
    private Label topicText;
    @FXML
    private Label posText;
    @FXML
    private TextField authorText;
    @FXML
    private TextArea postContent;
    @FXML
    private Button addButton;

    private Topic topic;

    private Integer position;

    public void initialize(){
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                addPost();
            }
        });
    }

    protected void setTopic(Topic topic){
        this.topic = topic;
        topicText.setText(topic.getSubject());
    }
    protected void setPosition(Integer position){
        this.position = position;
        posText.setText(position.toString());
    }
    private void addPost(){
        var post = new Post();
        var databaseService = new DatabaseService();
        try{
            post.setUrl(databaseService.getCustomPostId());
            post.setTopic(topic);
            List<Post> posts = topic.getPosts();
            for(var p : posts){
                if(p.getPosition() >= position){
                    p.setPosition(p.getPosition()+1);
                }
            }
            post.setBody(postContent.getText());
            post.setAuthor(authorText.getText());
            post.setPosition(position);
            topic.getPosts().add(post);
            databaseService.mergeData(topic);
        } catch (NullPointerException ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Undefined error");
            alert.setContentText("Close this window and try again later");
            alert.showAndWait();
        }
    }
}
