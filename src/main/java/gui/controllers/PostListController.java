package gui.controllers;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import parser.db.DatabaseService;
import parser.models.elements.Post;
import parser.models.elements.Topic;

import java.io.IOException;
import java.util.List;


public class PostListController {
    @FXML
    private TableView tableView;
    @FXML
    private Label topicSubject;
    private Topic topic;

    public void initialize(){
        List<TableColumn> col =(List<TableColumn>) tableView.getColumns();
        col.get(0).setCellValueFactory(
                new PropertyValueFactory<Post,Integer>("position")
        );
        col.get(1).setCellValueFactory(
                new PropertyValueFactory<Post,String>("author")
        );
        col.get(2).setCellValueFactory(
                new PropertyValueFactory<Post,String>("bodyView")
        );
        var contextMenu = getContextMenu();
        tableView.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent contextMenuEvent) {
                contextMenu.show(tableView,contextMenuEvent.getScreenX(),contextMenuEvent.getScreenY());
            }
        });
    }
    protected void setTopic(Topic topic){
        this.topic = topic;
        topicSubject.setText(topic.getSubject());
        loadPosts();
    }

    private void loadPosts(){
        for(var post : topic.getPosts()){
            var body = post.getBody();
            for(int i=70; i < body.length(); i+=70){
                body = body.substring(0,i) + '\n' + body.substring(i,body.length());
                i++;
            }
            post.setBodyView(body);
        }
        tableView.setItems(FXCollections.observableArrayList(topic.getPosts()));
    }

    private ContextMenu getContextMenu(){
        var result = new ContextMenu();
        MenuItem deletePost = new MenuItem("Delete");
        deletePost.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                var post = (Post) tableView.getSelectionModel().getSelectedItem();
                delete(post);
            }
        });
        MenuItem addAbove = new Menu("Add new post above");
        addAbove.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent actionEvent) {
                 var post = (Post) tableView.getSelectionModel().getSelectedItem();
                 add(post.getPosition());
             }
        });
        MenuItem addBelow = new Menu("Add new post below");
        addBelow.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    var post = (Post) tableView.getSelectionModel().getSelectedItem();
                    add(post.getPosition()+1);
                }

            });
        result.getItems().add(deletePost);
        result.getItems().add(addAbove);
        result.getItems().add(addBelow);
        return result;
    }
    private void add(Integer position){
        Stage parserStage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(
                getClass()
                        .getResource("/fxml/PostCreator.fxml"
                        )
        );
        try {
            parserStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        PostCreatorController controller = loader.<PostCreatorController>getController();
        controller.setTopic(topic);
        controller.setPosition(position);
        parserStage.showAndWait();
        loadPosts();

    }
    private void delete(Post post){
        var databaseService = new DatabaseService();
        databaseService.deletePost(post);
        topic.getPosts().remove(post);
        loadPosts();
    }
}
