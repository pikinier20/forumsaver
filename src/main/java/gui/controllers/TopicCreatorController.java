package gui.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import parser.db.DatabaseService;
import parser.models.elements.Forum;
import parser.models.elements.Post;
import parser.models.elements.Topic;

public class TopicCreatorController {
    @FXML
    private Label rootName;
    @FXML
    private Label forumName;
    @FXML
    private TextField subjectText;
    @FXML
    private TextArea textArea;
    @FXML
    private Button addButton;
    @FXML
    private TextField authorText;

    private Forum forum;

    public void initialize(){
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                add();
            }
        });
    }

    protected void setForum(Forum forum){
        var databaseService = new DatabaseService();
        this.forum = forum;
        this.rootName.setText(databaseService.getRootName(forum));
        this.forumName.setText(forum.getName());
    }
    private void add(){
        if(subjectText.getText().isEmpty() || textArea.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Form not filled properly");
            alert.setContentText("Make sure you did not left any input empty");
            alert.showAndWait();
        }
        try{
            var databaseService = new DatabaseService();
            var id = databaseService.getCustomTopicId();
            var topic = new Topic(id,subjectText.getText());
            var post = new Post(authorText.getText(),0,textArea.getText(),databaseService.getCustomPostId());
            topic.setForum(forum);
            topic.getPosts().add(post);
            post.setTopic(topic);
            databaseService.mergeData(topic);
            Stage stage = (Stage) addButton.getScene().getWindow();
            stage.close();

        } catch(NullPointerException ex){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Forum not selected");
            alert.setContentText("Quit this window and retry later");
            alert.showAndWait();
        }
    }
}
