package gui.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import parser.db.DatabaseService;
import parser.models.elements.Forum;
import parser.models.elements.Topic;

import java.io.IOException;

public class TopicListController {
    @FXML
    private ListView<Topic> dataTable;
    @FXML
    private Label forumName;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    private Forum forum;
    public void initialize(){
        dataTable.setCellFactory(new Callback<ListView<Topic>, ListCell<Topic>>() {
            @Override
            public ListCell<Topic> call(ListView<Topic> topicListView) {
                return new ListCell<Topic>(){
                    @Override
                    protected void updateItem(Topic topic, boolean b) {
                        super.updateItem(topic, b);
                        if(topic == null)
                        {
                            setText(null);
                        }
                        else {
                            setText(topic.getSubject());
                        }
                    }
                };
            }
        });
        dataTable.setMouseTransparent(false);
        dataTable.setFocusTraversable(true);
        dataTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){
                        var topic = dataTable.getSelectionModel().getSelectedItem();
                        showPosts(topic);
                    }
                }

            }
        });
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                addNewTopic();
            }
        });
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                var topic = dataTable.getSelectionModel().getSelectedItem();
                delete(topic);
            }
        });
    }
    protected void setForum(Forum forum){
        this.forum = forum;
        this.forumName.setText(forum.getName());
        loadData();
    }
    private void addNewTopic(){
        Stage parserStage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(
                getClass()
                        .getResource("/fxml/TopicCreator.fxml"
                        )
        );
        try {
            parserStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        TopicCreatorController controller = loader.<TopicCreatorController>getController();
        controller.setForum(forum);
        parserStage.show();
    }
    private void delete(Topic topic){
        var databaseService = new DatabaseService();
        databaseService.deleteTopic(topic);
        forum.getTopics().remove(topic);
        loadData();
    }
    private void loadData(){
        dataTable.setItems(FXCollections.observableArrayList(forum.getTopics()));

    }
    private void showPosts(Topic topic){
        Stage parserStage = new Stage(StageStyle.DECORATED);
        FXMLLoader loader = new FXMLLoader(
                getClass()
                        .getResource("/fxml/PostList.fxml"
                        )
        );
        try {
            parserStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        PostListController controller = loader.<PostListController>getController();
        controller.setTopic(topic);
        parserStage.show();
    }
}
