package gui.controllers;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import parser.DataParser;
import parser.db.DatabaseService;
import parser.models.elements.Forum;

public class TopicParserController{
    @FXML
    private Button startButton;
    @FXML
    private Label rootName;
    @FXML
    private Label forumName;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label progressText;

    private Forum forum;

    public void initialize(){
        startButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                var task = getParseTask();
                progressBar.progressProperty().bind(task.progressProperty());
                progressText.textProperty().bind(task.messageProperty());
                new Thread(task).start();
            }
        });
    }

    protected void setForum(Forum forum){
        var databaseService = new DatabaseService();
        this.forum = forum;
        rootName.setText(databaseService.getRootName(forum));
        forumName.setText(forum.getName());
    }

    private Task getParseTask(){
        Task task = new Task(){
            @Override
            protected Object call() throws Exception {
                var dataParser = new DataParser();
                var databaseService = new DatabaseService();
                var rootUrl = databaseService.getRootUrl(forum);
                updateProgress(10,100);
                updateMessage("Loading topics from site...");
                dataParser.parseTopics(forum,rootUrl);
                updateProgress(20,100);
                updateMessage("Adding topics to database...");
                databaseService.mergeTopicList(forum);
                updateProgress(50,100);
                updateMessage("Loading posts from site...");
                dataParser.parsePosts(forum,rootUrl);
                updateProgress(80,100);
                updateMessage("Saving posts in database...");
                databaseService.mergeTopicList(forum);
                updateProgress(100,100);
                updateMessage("Success!");
                return null;
            }
        };
        return task;
    }

}
