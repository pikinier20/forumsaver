package parser;

import parser.db.DatabaseService;
import parser.helpers.HttpService;
import parser.models.elements.Forum;
import parser.models.elements.Topic;
import parser.models.pages.ForumPage;
import parser.models.pages.TopicPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class DataParser {
    ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
    HttpService dataGetter = new HttpService();
    private class AsyncRecursiveForumLoader extends RecursiveTask<Forum>{
        String url;
        String baseUrl;
        Forum forum;
        Forum parentForum;
        List<String> subforumLinks;

        protected AsyncRecursiveForumLoader(String url, String baseUrl,Forum parentForum){
            this.url = url;
            this.baseUrl = baseUrl;
            this.parentForum = parentForum;
        }

        @Override
        protected Forum compute () {
            String html = null;
            try {
                html = dataGetter.getData(url);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(html.equals("") || html == null) return null;
            ForumPage firstPage = new ForumPage(html,baseUrl);
            forum = new Forum(firstPage);
            subforumLinks = firstPage.getSubforumLinks();
            forum.setForumUrl(url);
            forum.setForum(parentForum);
            forum.setName(firstPage.getForumTitle());
            var subtasks = createSubtasks();

            for(AsyncRecursiveForumLoader subtask : subtasks){
                subtask.fork();
            }
            for(AsyncRecursiveForumLoader subtask : subtasks){
                if(subtask.join() != null) {
                    forum.getSubforums().add(subtask.join());
                }
            }
            return forum;
        }
        private List<AsyncRecursiveForumLoader> createSubtasks(){
            var subtasks = new ArrayList<AsyncRecursiveForumLoader>();
            for(String link : subforumLinks){
                subtasks.add(new AsyncRecursiveForumLoader(link,baseUrl,forum));
            }
            return subtasks;
        }
    }
    private class AsyncRecursiveNewForumParser extends RecursiveTask<Forum> {
        String url;
        protected AsyncRecursiveNewForumParser(String url){
            this.url = url;
        }
        @Override
        protected Forum compute() {
            Forum result = null;
            try {
                result = parseForum();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return result;
        }

        public Forum parseForum() throws IOException, InterruptedException {
            String html = null;
            try {
                html = dataGetter.getData(url);
            } catch(IllegalArgumentException ex){
                return null;
            }
            ForumPage mainPage = new ForumPage(html, url);
            if(mainPage.getSubforumLinks().isEmpty()) {
                return null;
            }
            Forum main = new Forum(mainPage);
            main.setForum(null);
            main.setForumUrl(url);
            main.setName(mainPage.getForumTitle());
            System.out.println("aa");
            ArrayList<AsyncRecursiveForumLoader> tasks = new ArrayList<>();
            for (String link : main.getPages().get(0).getSubforumGroupLinks()) {
                var task = new AsyncRecursiveForumLoader(link, url,main);
                tasks.add(task);
                task.fork();
            }
            for (var task : tasks) {
                main.getSubforums().add(task.join());
            }
            return main;

        }
    }
    private class AsyncRecursiveTopicLoader extends RecursiveAction {
        Forum forum;
        String baseUrl;
        ExecutorService executorService;

        public AsyncRecursiveTopicLoader(Forum forum, String baseUrl, ExecutorService executorService) {
            this.forum = forum;
            this.baseUrl = baseUrl;
            this.executorService = executorService;
        }

        @Override
        protected void compute() {
            var subtasks = new LinkedList<AsyncRecursiveTopicLoader>();
            for (var subforum : forum.getSubforums()) {
                subtasks.add(new AsyncRecursiveTopicLoader(subforum, baseUrl, executorService));
            }
            for (var subtask : subtasks) {
                subtask.fork();
            }
            for (var subtask : subtasks) {
                subtask.join();
            }
            this.forum.setTopics(new LinkedList<>());
            List<String> pageLinks = null;
            try {
                pageLinks = ForumPage.parseForumPage(dataGetter.getData(forum.getForumUrl()),baseUrl).getPagesList();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            var debug = 2;
            pageLinks.forEach(a -> {
                var task = (Runnable) () -> {
                    List<Topic> topics = new LinkedList();
                    ForumPage page = null;
                    try {
                        page = new ForumPage(dataGetter.getData(a), baseUrl);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    topics = page.getTopicList();
                    for (var topic : topics) {
                        topic.setForum(forum);
                    }
                    forum.getTopics().addAll(topics);
                };
                executorService.submit(task);
            });
        }
    }
    private class AsyncRecursivePostLoader extends RecursiveAction {
        private Forum forum;
        private String baseUrl;
        ExecutorService executorService;
        public AsyncRecursivePostLoader(Forum forum, String baseUrl, ExecutorService executorService) {
            this.forum  = forum;
            this.baseUrl = baseUrl;
            this.executorService = executorService;
        }

        @Override
        public void compute() {
            var subtasks = new LinkedList<AsyncRecursivePostLoader>();
            for(var subforum : forum.getSubforums()){
                subtasks.add(new AsyncRecursivePostLoader(subforum,baseUrl,executorService));
            }
            for(var subtask : subtasks){
                subtask.fork();
            }
            for(var subtask : subtasks){
                subtask.join();
            }
            for(var topic : forum.getTopics()) {
                executorService.submit(new Thread(() -> {
                    try {
                        loadPosts(topic, baseUrl);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }));
            }
        }

        public void loadPosts(Topic topic, String baseUrl) throws IOException, InterruptedException {
            try {
                var topicPage = new TopicPage(dataGetter.getData(topic.getUrl()), baseUrl);
                for (var link : topicPage.getPagesList()) {
                    var page = new TopicPage(dataGetter.getData(link), baseUrl);
                    var posts = page.getPosts();
                    posts.forEach(p -> {
                        p.setTopic(topic);
                        p.setUrl(topic.getUrl() + p.getUrl());
                    });
                    topic.getPosts().addAll(posts);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public Forum parseForumStructure(String url) throws InterruptedException {
        var result = forkJoinPool.invoke(new AsyncRecursiveNewForumParser(url));
        return result;
    }
    public void parseTopics(Forum forum,String rootUrl) throws InterruptedException, IOException {
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        var topicLoader = new AsyncRecursiveTopicLoader(forum,rootUrl,executorService);
        forkJoinPool.invoke(topicLoader);
        executorService.shutdown();
        executorService.awaitTermination(3600, TimeUnit.SECONDS);
    }

    public void parsePosts(Forum forum,String rootUrl) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        var postLoader = new AsyncRecursivePostLoader(forum,rootUrl,executorService);
        forkJoinPool.invoke(postLoader);
        executorService.shutdown();
        executorService.awaitTermination(3600,TimeUnit.SECONDS);
    }

}

