package parser.db;

import parser.models.elements.Forum;
import parser.models.elements.Post;
import parser.models.elements.Topic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.util.*;

public class DatabaseService {
    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("ForumsDB");
    private EntityManager entityManager;
    public DatabaseService(){
        entityManager = factory.createEntityManager();
    }
    public List<Forum> getRootNodes(){
        entityManager.getTransaction().begin();
        List<Forum> result;
        result = entityManager.createQuery("SELECT f FROM Forum f WHERE f.forum IS NULL").getResultList();
        entityManager.getTransaction().commit();
        return result;
    }
    public void mergeData(Object o){
        entityManager.getTransaction().begin();
        entityManager.merge(o);
        entityManager.getTransaction().commit();
    }
    public void mergeTopicList(Forum forum){
        entityManager.getTransaction().begin();
        forum.getTopics().forEach(p -> {
            entityManager.merge(p);
            p.getPosts().forEach(q -> {
                        entityManager.merge(q);
                    }
            );
            }
        );
        entityManager.getTransaction().commit();
        for(var subforum : forum.getSubforums()){
            mergeTopicList(subforum);
        }
    }
    public String getRootUrl(Forum forum){
        var root = forum.getForum();
        while(root.getForum() != null){
            root = root.getForum();
        }
        return root.getForumUrl();
    }

    public String getRootName(Forum forum){
        var root = forum.getForum();
        while(root.getForum() != null){
            root = root.getForum();
        }
        return root.getName();
    }
    public Map<String,Long> getTopicCount(){
        entityManager.getTransaction().begin();
        var query = entityManager.createQuery("SELECT t.forum.id, count(t) from Topic t group by t.forum").getResultList();
        entityManager.getTransaction().commit();
        var result = new HashMap<String,Long>();
        for(var entry : query){
            var castedEntry = (Object[]) entry;
            result.put((String)castedEntry[0],(Long)castedEntry[1]);
        }
        return result;
    }
    public void deletePost(Post post){
        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery("DELETE from Post P where P.id = :id").setParameter("id",post.getUrl()).executeUpdate();
            entityManager.getTransaction().commit();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public void deleteTopic(Topic topic){
        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery("DELETE from Topic T where T.id = :id").setParameter("id",topic.getUrl()).executeUpdate();
            entityManager.getTransaction().commit();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public String getCustomTopicId() {
        entityManager.getTransaction().begin();
        List<String> result = entityManager.createQuery("SELECT id from Topic T where T.id like :custom").setParameter("custom", "%custom%").getResultList();
        entityManager.getTransaction().commit();
        return getCustomId(result);
    }
    public String getCustomPostId() {
        entityManager.getTransaction().begin();
        List<String> result = entityManager.createQuery("SELECT id from Post P where P.id like :custom").setParameter("custom", "%custom%").getResultList();
        entityManager.getTransaction().commit();
        return getCustomId(result);
    }
    private String getCustomId(List<String> result){
        if (result.isEmpty()) {
            return "custom1";
        }
        List<String> cutted = new LinkedList<>();
        for (var customId : result) {
            cutted.add(customId.substring(customId.indexOf("custom") + 6, customId.length()));
        }
        Integer max = 1;
        for (var customId : cutted) {
            try {
                int no = Integer.parseInt(customId);
                if (no > max) max = no;
            } catch (NumberFormatException ex) {
                continue;
            }
        }
        max++;
        return "custom" + max;
    }
}
