package parser.helpers;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class HttpService {
    String forumUrl;
    HttpClient.Builder builder;
    HttpClient httpClient;
    public HttpService(HttpClient.Builder builder){
        this.builder = builder;
        httpClient = builder
                .version(HttpClient.Version.HTTP_2)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build();
    }
    public HttpService(){
        this(HttpClient.newBuilder());
    }
    public String getData(String url) throws IOException, InterruptedException, IllegalStateException {
        HttpRequest request = null;
        request = HttpRequest.newBuilder().uri(URI.create(url)).GET().timeout(Duration.ofMinutes(1)).build();
        String response = null;
        Integer attempts = 0;
        while(response == null) {
            try {
                response = httpClient.send(request, HttpResponse.BodyHandlers.ofString()).body();
            } catch (IOException ex) {
                if(attempts > 15){
                    throw ex;
                }
                attempts++;
                response = null;
                continue;
            }
        }
        return response;
    }
//    public String getData(String url) throws IOException {
//        try {
//            String site = Jsoup.connect(url).get().toString();
//        } catch (HttpStatusException ex){
//
//        }
//        return site;
//    }

}
