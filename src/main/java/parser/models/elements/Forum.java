package parser.models.elements;

import parser.models.pages.ForumPage;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Forum")
@Table(name = "forum")
public class Forum implements Serializable {
    @Id
    private String forumUrl;
    private String name;

    @OneToMany(
            mappedBy = "forum",
            targetEntity = Forum.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    private List<Forum> subforums;
    @OneToMany(
            mappedBy = "forum",
            targetEntity = Topic.class,
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private List<Topic> topics;

    @Transient
    private List<ForumPage> pages;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subforums_id")
    private Forum forum;


    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getForumUrl() {
        return forumUrl;
    }

    public void setForumUrl(String forumUrl) {
        this.forumUrl = forumUrl;
    }

    public List<Forum> getSubforums() {
        return subforums;
    }

    public void setSubforums(List<Forum> subforums) {
        this.subforums = subforums;
    }

    public List<ForumPage> getPages() {
        return pages;
    }

    public void setPages(List<ForumPage> pages) {
        this.pages = pages;
    }


    public Forum(){
        subforums = new ArrayList<>();
        pages = new ArrayList<>();
    }
    public Forum(ForumPage firstPage) {
        this();
        pages.add(firstPage);
    }
}
