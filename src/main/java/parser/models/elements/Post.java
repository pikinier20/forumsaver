package parser.models.elements;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "post")
public class Post {
    @Id
    private String url;
    private String author;
    private Integer position;
    private String body;
    @Transient
    private String bodyView;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "posts_id")
    private Topic topic;
    public Post(){ }
    public Post(String author,Integer position,String body,String postUrl){
        this.author = author;
        this.position = position;
        this.body = body;
        this.url = postUrl;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getBodyView() {
        return bodyView;
    }

    public void setBodyView(String bodyView) {
        this.bodyView = bodyView;
    }

}
