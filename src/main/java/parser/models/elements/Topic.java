package parser.models.elements;

import parser.models.pages.TopicPage;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "topic")
public class Topic implements Serializable {
    @Id
    private String url;
    private String subject;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "topics_id")
    private Forum forum;
    @OneToMany(
            mappedBy = "topic",
            targetEntity = Post.class,
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private List<Post> posts;
    @Transient
    private List<TopicPage> pages;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public Topic(){

        pages = new LinkedList<TopicPage>();
        posts = new LinkedList<Post>();
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public Topic(String url, String subject){
        this();
        this.url = url;
        this.subject=subject;
    }
    public Topic(TopicPage firstPage){
        this();
        pages.add(firstPage);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj.getClass() != Topic.class) return false;
        var topicObj = (Topic) obj;
        return this.url == topicObj.url;
    }
}
