package parser.models.pages;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import parser.models.elements.Forum;
import parser.models.elements.Topic;

import javax.persistence.*;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

public class ForumPage extends Page {


    private Forum forum;

    public ForumPage(String html, String url){
        super(html,url);
    }

    public ArrayList<String> getSubforumLinks(){
        Elements subforumElems = pageContent.select(".inner .topiclist.forums .forumtitle");
        var subforumLinkList = new ArrayList<String>();
        for(Element elem: subforumElems){
            var link = elem.attr("abs:href");
            if(link.indexOf("&sid=") > 0){
                link = link.substring(0,link.indexOf("&sid="));
            }
            subforumLinkList.add(link);
        }
        subforumLinkList.removeIf(a -> a.indexOf("viewforum") < 0);
        subforumLinkList.removeIf(a -> a.indexOf(this.url) < 0);
        subforumLinkList.forEach(a -> {
            if(a.indexOf("&sid=") > 0){
                a = a.substring(0,a.indexOf("&sid="));
                System.out.println(a);
            }
        });

        return subforumLinkList;
    }
    public String getNextPageLink(){
        String result = pageContent.select("div.pagination li.arrow.next a.button").attr("abs:href");
        return result;
    }

    public List<String> getSubforumGroupLinks(){
        Elements subforumGroupElems = pageContent.select("div.forabg");
        var linkList = new ArrayList<String>();
        for(Element elem: subforumGroupElems){
            var link = elem.select(".inner .topiclist:not(.forums)  a").attr("abs:href");
            if(link.indexOf("&sid=") > 0){
                link = link.substring(0,link.indexOf("&sid="));
            }
            linkList.add(link);
//            linkList.add(link);
        }
        linkList.removeIf(a -> a.indexOf("viewforum") < 0);
        linkList.removeIf(a -> a.indexOf(this.url) < 0);


        return linkList;
    }
    public String getForumTitle(){
        var result = pageContent.select("h2.forum-title > a").text();
        if(result.equals("")) {
            result = pageContent.title();
        }
        return result;
    }
    public List<Topic> getTopicList(){
        var topics = pageContent.select(".forumbg .topiclist.topics li");
        LinkedList<Topic> result = new LinkedList();
        for(var topic : topics){
            var url = topic.select(".list-inner a.topictitle").attr("abs:href");
            if(url.indexOf("&sid=") > 0){
                    url = url.substring(0,url.indexOf("&sid="));
            }
            if(url.indexOf("viewtopic") < 0) {
                continue;
            }
            var subject = topic.select(".list-inner a.topictitle").text();
            result.add(new Topic(url,subject));
        }
        return result;
    }
    public static ForumPage parseForumPage(String html, String baseUrl){
        return new ForumPage(html,baseUrl);
    }

    @Override
    public List<String> getPagesList() {
        var linkList = super.getPagesList();
        linkList.removeIf(a -> a.indexOf("viewforum") < 0);
        return linkList;
    }
}
