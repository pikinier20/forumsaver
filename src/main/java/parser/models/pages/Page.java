package parser.models.pages;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.ArrayList;
import java.util.List;

public abstract class Page {

    public Document pageContent;
    public String url;
    public Page(String html, String url){

        this.pageContent = Jsoup.parse(html,url);
        this.url = url;
    }

    public List<String> getPagesList(){
        Elements pages = pageContent.select("div.action-bar > div.pagination li");
        Elements pages2 = pageContent.select("div.topic-actions > div.pagination a");
        if(pages2.size() > pages.size()) pages = pages2;
        ArrayList<String> linkList = new ArrayList<String>();
        String maxLink = pageContent.select("link[rel='canonical']" ).attr("href") + "&start=0";
        Integer maxSite = 0;
        if(!pages.isEmpty()) {
            for (Element page : pages) {
                try {
                    Integer siteNumber = Integer.parseInt(page.text());
                    if(siteNumber > maxSite){
                        maxLink = page.select("a").attr("abs:href");
                        maxSite = siteNumber;
                    }
                }
                catch(NumberFormatException ex){
                    continue;
                }
            }
        }
            Integer elementsPerPage = Integer.parseInt(maxLink.substring(maxLink.lastIndexOf("start=")+6))/(maxSite-1);
            Integer inc = 1;
            String link = maxLink.substring(0,maxLink.lastIndexOf("start=")+6);
            linkList.add(link+'0');
            while(inc < maxSite){
                linkList.add(link+(inc*elementsPerPage));
                inc++;
            }

        return linkList;
    }
}
