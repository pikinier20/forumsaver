package parser.models.pages;

import org.jsoup.select.NodeFilter;
import parser.models.elements.Post;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TopicPage extends Page {
    public TopicPage(String html, String url){
        super(html,url);
    }

    public List<Post> getPosts(){
        var posts = pageContent.select(".post");
        var position = 0;
        if(this.url.contains("start=")){
            var offset = Integer.parseInt(this.url.substring(this.url.indexOf("start=")+6));
            position = position + offset;
        }
        var result = new LinkedList<Post>();
        for(var post:posts){
            var author = post.select(".postbody .author strong a").text();
            var body = post.select(".postbody .content").text();
            var postNumber = post.select(".postbody > div > h3 > a").attr("href");
            try {
                var postObject = new Post(author, position, body, postNumber);
                result.add(postObject);
            }
            catch(Exception e){
                e.printStackTrace();
            }
            position++;
        }
        return result;
    }

    @Override
    public List<String> getPagesList() {
        var linkList = super.getPagesList();
        linkList.removeIf(a -> a.indexOf("viewtopic") < 0);
        return linkList;
    }
}
